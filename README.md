## Введение
[![IMG](https://1.downloader.disk.yandex.ru/preview/9ad6c7ecf052492fdea81498145a766fca53877a88f403b2e28d39df644f2e2b/inf/O9L1-u4wzaSotV9caCiCGc0jFqf6Dkx7NfV4DXwb1kODkh-UeGBaZuTuqFsT-IXkfWGACv3uXqkxF3WL9z3LxQ%3D%3D?uid=0&filename=2015-09-30%2016-45-08%20Modx%20Versioning%20Tool.%20Part1%20-%20JasperGrimm%27s%20library.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&tknv=v2&size=XXL&crop=0)](http://www.screencast.com/t/qs4DzpEHr5e)

## Use cases
[![IMG](https://3.downloader.disk.yandex.ru/preview/0004eece3ffda21af81f5c6d84b69987bbcf8890b1e37e5951713233716c5771/inf/kMfN0pHFKqvEDmWWF8xTT2C_ONNgTeSyRGdtb0z4aq3duAAnrcDPWJT3BfsH_ZZGxEVlyMNaZBvrkepfF8Alcw%3D%3D?uid=0&filename=2015-09-30%2016-50-25%20Modx%20Versioning%20Tool.%20Part2%20-%20JasperGrimm%27s%20library.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&tknv=v2&size=XXL&crop=0)](http://www.screencast.com/t/mC1m4tbmp)

## Установка

В composer.json добавить зависимость:

```
{
    "require-dev": {
        ....
        "grimmlab/modvert": "0.1"
    },
    "repositories": [
        {
            "type": "package",
            "package": {
                "name": "grimmlab/modvert",
                "version": "0.1",
                "source": {
                    "url": "https://drwhite@bitbucket.org/drwhite/modvert.git",
                    "type": "git",
                    "reference": "origin/master"
                },
                "autoload": {
                    "psr-0": {
                      "Qst": "src/src/"
                    },
                    "psr-4": {
                      "Qst\\": "src/src"
                    }
                }
            }    
        }
    ],
    ...
    
    "config": {
        "bin-dir": "./"
    }
}
```
Далее 
```
composer update
```

В корне проекта создаем файл modvert.yml, содержащий:

```
bootstrap: ""
database:
  host: "localhost"
  port: 3306
  user: "questoria"
  password: "questoria"
  name: "akorsun_questoria_debug"
  prefix: "qst_"
storage: `APP_PATH . "storage/"`
default_stage: 'test'
stages:
    staging:
        remote_url: 'http://staging.questoria.ru'
    test:
        remote_url: 'http://test.questoria.ru'
    development:
        remote_url: 'http://modvert.loc/sample_project'
```

В этом файле вам надо изменить настройки подключения к БД. Остальное трогать необходимости нет. 

Далее
```
./modvert install
```

## Запуск
```
./modvert
```

**Доступные команды:**

1.  Инициализация Modvert для текущего проекта. Добавляет служебные таблицы в БД.
    
    ```
    ./modvert install
    ```
    
2.  Очистка историии синхронизаций  
    
    ```
    ./modvert clean
    ```
    
3.  Загрузка содержимого файлов в **локальную** БД
    
    ```
    ./modvert load
    ```
    
    Необходимо, когда сделали какие-то правки в файлах моделей и хотите применить правки. Так же эта команда необходима, когда при синхронизации возникает конфликт. Вы решаете конфликт, а затем загружаете с помощью этой команды результат в базу данных. 
4.  Сихронизация ресурсов между локальной и удаленной копией.

    ```
    ./modvert update --stage=staging
    ```
    
    или
    
    ```
    ./modvert update --stage=test
    ```
