<?php
// This is global bootstrap for autoloading
include __DIR__ . '/../src/bootstrap.php';

$args['context'] = 'cli';
$app->bind('request', function() { return null; });
$app->bind('response', function() { return null; });

\Qst\App::init($app, $conf);