<?php 
$I = new UnitTester($scenario);
$I->wantTo('perform actions and see result');



$resourceManager = new \Qst\ResourceManager();
$remote_url = \Qst\App::config('stages.development.remote_url');
$resourceManager->setDriver(new \Qst\Driver\DatabaseDriver($remote_url));

$tv_db = $resourceManager->findOneByName(\Qst\IModxResource::TYPE_TEMPLVAR, 'Scenario');
$I->assertNotContains("1", $tv_db->getTemplates());

$tv_file = \Qst\ResourceModel::create(\Qst\IModxResource::TYPE_TEMPLVAR);
$tv_file->loadFromFile($tv_db->getBoundFile());

$tv_data = $tv_db->toArray();
$tv_data['templates'][] = 1;

$tv_db->loadFromArray($tv_data);
$resourceManager->update($tv_db);

$tv_db = $resourceManager->findOneByName(\Qst\IModxResource::TYPE_TEMPLVAR, 'Scenario');
$I->assertContains(1, $tv_db->getTemplates());

$tv_data = $tv_db->toArray();
$tv_data['templates'] = [11];

$tv_db->loadFromArray($tv_data);
$resourceManager->update($tv_db);

$tv_db = $resourceManager->findOneByName(\Qst\IModxResource::TYPE_TEMPLVAR, 'Scenario');
$I->assertNotContains(1, $tv_db->getTemplates());