<?php 
$I = new UnitTester($scenario);
$I->wantTo('perform actions and see result');



$resourceManager = new \Qst\ResourceManager();
$remote_url = \Qst\App::config('stages.development.remote_url');
$resourceManager->setDriver(new \Qst\Driver\DatabaseDriver($remote_url));

$category = $resourceManager->findOneByName(\Qst\IModxResource::TYPE_CATEGORY, 'Questoria Templates');

$category->serialize();

$I->assertEquals('Questoria Templates', $category->getName());

$f_category = \Qst\ResourceModel::create(\Qst\IModxResource::TYPE_CATEGORY);
$f_category->loadFromFile($category->getBoundFile());
$I->assertEquals($f_category->getName(), $category->getName());