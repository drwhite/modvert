<?php 
$I = new UnitTester($scenario);
$I->wantTo('perform actions and see result');

$resourceManager = new \Qst\ResourceManager();
$remote_url = \Qst\App::config('stages.development.remote_url');
$resourceManager->setDriver(new \Qst\Driver\RemoteDriver($remote_url));

$chunk = $resourceManager->findOneByName(\Qst\IModxResource::TYPE_CHUNK, 'CardSert');

$I->assertTrue($chunk instanceof \Qst\IModxResource);
$I->assertEquals('CardSert', $chunk->getName());

$expected_content = <<<'C'
<div class="cert-home">
	<a href="/[~49~]">Квест в подарок<span class="bow"></span></a>
</div>
C;

$I->assertEquals($expected_content, $chunk->getContent());

$chunk->serialize();

$new_content = <<<'C'
<!-- local update -->
<div class="cert-home">
	<a href="/[~49~]">Квест в подарок<span class="bow"></span></a>
</div>
C;

$chunk_data = $chunk->toArray();
$chunk_data['snippet'] = $new_content;
$chunk->loadFromArray($chunk_data);
$resourceManager->update($chunk);
$chunk->serialize();
$chunk = $resourceManager->findOneByName(\Qst\IModxResource::TYPE_CHUNK, 'CardSert');

$I->assertTrue($chunk instanceof \Qst\IModxResource);
$I->assertEquals('CardSert', $chunk->getName());
$I->assertEquals($new_content, $chunk->getContent());

$chunk_data = $chunk->toArray();
$chunk_data['snippet'] = $expected_content;
$chunk->loadFromArray($chunk_data);
$resourceManager->update($chunk);
$chunk->serialize();

/**
 * Теперь построим чанк из привязанного файла и сравним его контент с контентом
 * чанка из БД
 */
$chunk_from_file = \Qst\ResourceModel::create(\Qst\IModxResource::TYPE_CHUNK);
$chunk_from_file->loadFromFile($chunk->getBoundFile());
$I->assertEquals($chunk->getContent(), $chunk_from_file->getContent());