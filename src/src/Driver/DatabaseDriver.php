<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 15.09.15
 * Time: 16:46
 */

namespace Qst\Driver;


use Qst\Command\Command;
use Qst\Log;
use Qst\IModxResource;
use Qst\IModxResourceDriver;
use Illuminate\Database\Capsule\Manager as DB;
use Qst\ResourceModel;

class DatabaseDriver implements IModxResourceDriver
{

    static $tables_map = [
        IModxResource::TYPE_CHUNK => 'modx_site_htmlsnippets',
        IModxResource::TYPE_SNIPPET => 'modx_site_snippets',
        IModxResource::TYPE_CONTENT => 'modx_site_content',
        IModxResource::TYPE_TEMPLATE => 'modx_site_templates',
        IModxResource::TYPE_TEMPLVAR => 'modx_site_tmplvars',
        IModxResource::TYPE_CATEGORY => 'modx_categories',
    ];

    const LOCK_CONTENT_ACTION = 27;
    const LOCK_CHUNK_ACTION = 78;
    const LOCK_SNIPPET_ACTION = 22;
    const LOCK_TEMLATE_ACTION = 16;
    const LOCK_CATEGORY_ACTION = 0;

    const LOCAL_USER_ID = 777777777;
    const LOCAL_USER_NAME = 'agent-007';

    protected $locks_map = [
        IModxResource::TYPE_CHUNK => DatabaseDriver::LOCK_CHUNK_ACTION,
        IModxResource::TYPE_SNIPPET => DatabaseDriver::LOCK_SNIPPET_ACTION,
        IModxResource::TYPE_CONTENT => DatabaseDriver::LOCK_CONTENT_ACTION,
        IModxResource::TYPE_TEMPLATE => DatabaseDriver::LOCK_TEMLATE_ACTION,
        IModxResource::TYPE_CATEGORY => DatabaseDriver::LOCK_CATEGORY_ACTION,
    ];

    public static function getTableNameByType($type)
    {
        return self::$tables_map[$type];
    }

    protected function mutate($type, $item)
    {
        if (IModxResource::TYPE_TEMPLVAR === $type) {
            $templates = DB::table('modx_site_tmplvar_templates')
                ->where('tmplvarid', '=', $item['id'])
                ->get(['templateid']);
            $templates = array_map(function($item){
                return $item['templateid'];
            }, $templates);
            $item['templates'] = $templates;
            $content_values = DB::table('modx_site_tmplvar_contentvalues')
                ->where('tmplvarid', '=', $item['id'])
                ->orderBy('contentid')
                ->get(['contentid', 'value'])
            ;
            $item['content_values'] = [];
            foreach ($content_values as $cv) {
                $item['content_values'][$cv['contentid']] = addslashes($cv['value']);
            }
        }
        return $item;
    }

    public function getCollection($type)
    {
        $tablename = self::$tables_map[$type];
        $collection = DB::table($tablename)->get();
        if (IModxResource::TYPE_TEMPLVAR === $type) {
            $collection = array_map(function($item) use($type) {
                return $this->mutate($type, $item);
            }, $collection);
            return $collection;
        }
        return $collection;
    }

    public function find($type, $pk)
    {
        return $this->mutate($type, DB::table(self::$tables_map[$type])->find($pk));
    }

    public function findOneByName($type, $name)
    {
        $key = 'name';
        switch ($type) {
            case IModxResource::TYPE_CHUNK:
            case IModxResource::TYPE_SNIPPET:
            case IModxResource::TYPE_TEMPLVAR:
                $key = 'name';
                break;
            case IModxResource::TYPE_CONTENT:
                $key = 'alias';
                break;
            case IModxResource::TYPE_TEMPLATE:
                $key = 'templatename';
                break;
            case IModxResource::TYPE_CATEGORY:
                $key = 'category';
                break;
        }
        return $this->mutate($type, DB::table(self::$tables_map[$type])->where($key, '=', $name)->first());
    }

    public function delete($type, $id)
    {
        return DB::table(self::getTableNameByType($type))->delete($id);
    }

    protected function lock(IModxResource $resource)
    {

        $action = $this->locks_map[$resource->getType()];
        if ($record = DB::table('modx_active_users')
            ->where('internalKey', '=', self::LOCAL_USER_ID)->first()) {
            DB::table('modx_active_users')
            ->update([
                'action' => $action,
                'id' => $resource->getId(),
                'ip' => '127.0.0.1',
                'username' => self::LOCAL_USER_NAME,
                'lasthit' => time()
            ]);
        } else {
            DB::table('modx_active_users')
            ->insert([
                'internalKey' => self::LOCAL_USER_ID,
                'action' => $action,
                'id' => $resource->getId(),
                'ip' => '127.0.0.1',
                'username' => self::LOCAL_USER_NAME,
                'lasthit' => time()
            ]);
        }
        return true;
    }

    protected function unlock(IModxResource $resource)
    {
        $action = $this->locks_map[$resource->getType()];
        DB::connection()->delete('DELETE FROM modx_active_users WHERE action=' . $action . ' AND id=' . $resource->getId());
        return true;
    }

    public function update(IModxResource $resource)
    {
        $type = $resource->getType();
        $resource_data = $resource->toArray();
        if (IModxResource::TYPE_TEMPLVAR === $type) {
            // особая процедура обновления для переменных шаблона
            $templates = $resource_data['templates'];
            $content_values = $resource_data['content_values'];
            unset($resource_data['templates'], $resource_data['content_values']);
            $tv = DB::table(self::$tables_map[$type])->find($resource_data['id']);
            if (!$tv) {
                $tv = DB::table(self::$tables_map[$type])->find(
                    DB::table(self::$tables_map[$type])
                        ->insertGetId($resource_data)
                );
            }
            $tv_templates = DB::table('modx_site_tmplvar_templates')
                ->where('tmplvarid', '=', $tv['id'])
                ->get()
            ;
            $tv_templates = array_map(function($item){
                return $item['templateid'];
            }, $tv_templates);

            // получаем различия и отвязываем эти шаблоны от параметра
            $diff = array_diff($tv_templates, $templates);
            foreach ($diff as $t) {
                DB::table('modx_site_tmplvar_templates')
                    ->where('tmplvarid', '=', $tv['id'])
                    ->where('templateid', '=', $t)
                    ->delete();
            }

            foreach ($templates as $tid) {
                $exists = DB::table('modx_site_tmplvar_templates')
                    ->where('tmplvarid', '=', $tv['id'])
                    ->where('templateid', '=', $tid)
                    ->first()
                ;
                if (!$exists) {
                    DB::table('modx_site_tmplvar_templates')
                        ->insert([
                            'tmplvarid' => $tv['id'],
                            'templateid' => $tid
                        ])
                    ;
                }
            }

            foreach ($content_values as $content_id=>$value) {
                $exists = DB::table('modx_site_tmplvar_contentvalues')
                    ->where('tmplvarid', '=', $tv['id'])
                    ->where('contentid', '=', $content_id)
                    ->first()
                ;
                if (!$exists) {
                    DB::table('modx_site_tmplvar_contentvalues')
                        ->insert([
                            'tmplvarid' => $tv['id'],
                            'contentid' => $content_id,
                            'value' => $value
                        ])
                    ;
                } else {
                    DB::table('modx_site_tmplvar_contentvalues')
                        ->where('id', '=', $exists['id'])
                        ->update([
                            'value' => $value
                        ])
                    ;
                }
            }
            $upd = true;
        } else {
            $this->lock($resource);
            if (!DB::table(self::$tables_map[$type])->find($resource_data['id'])) {
                $upd = DB::table(self::$tables_map[$type])->insertGetId($resource_data);
            } else {
                $sql = 'update ' . self::$tables_map[$type] . ' SET ' . $resource->prepareFields() . ' WHERE id = ' . $resource_data['id'];
                Log::info($sql);
                $upd = DB::connection()->update($sql);
            }
            $this->unlock($resource);
        }

        Log::debug('Resource ' . $resource->getName() . ' update ' . ($upd ? 'successful' : 'fail'));
        return $upd;
    }

    public function batchUpdate($resources)
    {
        $result = true;
        foreach ($resources as $resource) {
            $r = \Qst\ResourceModel::create($resource['type'], $resource['data']);
            $result &= (bool)$this->update($r);
        }
        $resp = ['status' => 'OK'];
        if ($resp['status'] == 'OK') {
            \Qst\App::console()->writeln('<info>Ресурсы на удаленном сервере обновлены успешно!</info>');
            return true;
        } else {
            \Qst\App::console()->writeln('<error>Ресурсы на удаленном сервере не удалось обновить!</error>');
            return false;
        }
    }
}