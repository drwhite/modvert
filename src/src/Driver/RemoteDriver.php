<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 15.09.15
 * Time: 16:50
 */

namespace Qst\Driver;


use Qst\App;
use Qst\IModxResourceDriver;
use Qst\IModxResource;
use Qst\ResourceModel;


class RemoteDriver implements IModxResourceDriver
{

    protected $remote_url;

    static $tables_map = [
        IModxResource::TYPE_CHUNK => 'modx_site_htmlsnippets',
        IModxResource::TYPE_SNIPPET => 'modx_site_snippets',
        IModxResource::TYPE_CONTENT => 'modx_site_content',
        IModxResource::TYPE_TEMPLATE => 'modx_site_templates',
        IModxResource::TYPE_TEMPLVAR => 'modx_site_tmplvars',
    ];

    public function __construct($remote_url=null)
    {
        if (!$remote_url) {
            $stages = App::config('stages');
            $remote_url = $stages[App::config('default_stage')]['remote_url'];
        }
        $this->remote_url = $remote_url;
    }

    private function getContents($url)
    {
        $client = new \Guzzle\Http\Client();
        $request = $client->get($this->remote_url . $url, ['Content-Type'=>'application/json']);
        return $request->send()->getBody(true);
    }

    public function getCollection($type)
    {
        $content = json_decode(
            $this->getContents('/modvert.php?cmd=resource.get&type=' . $type),
            true
        );
        return $content;
    }

    public function delete($type, $id)
    {
        $client = new \Guzzle\Http\Client();
        $req = $client->delete($this->remote_url . '/modvert.php?cmd=resource.set', [
            'Content-Type'=>'application/x-www-form-urlencoded'
        ], http_build_query([
                'data'=> [
                    "tablename" => self::$tables_map[$type],
                    "type" => $type,
                    'id' => $id
                ]
            ])
        );
        $resp = $req->send();
        dd($resp->getBody(true));
    }

    public function update(IModxResource $resource)
    {
        if ('testing' === \Qst\App::make('env')) {
            return true;
        } else {
            $client = new \Guzzle\Http\Client();
            $req = $client->post($this->remote_url . '/modvert.php?cmd=resource.set', [
                'Content-Type'=>'application/x-www-form-urlencoded'
                ], http_build_query([
                    'data'=> [
                        [
                            "tablename" => $resource->getTableName(),
                            "type" => $resource->getType(),
                            "data" => $resource->toArray()
                        ]
                    ]
                ])
            );
            $resp = $req->send();
            return $resp->getBody(true);
        }        
    }

    public function batchUpdate($resources) 
    {
        $client = new \Guzzle\Http\Client();
        $req = $client->post($this->remote_url . '/modvert.php?cmd=resource.set', [
            'Content-Type'=>'application/x-www-form-urlencoded'
        ], http_build_query([
            'data'=> $resources
        ]));

        $resp = $req->send();

        $resp = json_decode($resp->getBody(true), true);

        if ($resp['status'] == 'OK') {
            App::console()->writeln('<info>Ресурсы на удаленном сервере обновлены успешно!</info>');
            return true;
        } else {
            App::console()->writeln('<error>Ресурсы на удаленном сервере не удалось обновить!</error>');
            return false;
        }
    }

    private function findOneByKey($type, $key, $value)
    {
        $collection = $this->getCollection($type);
        $collection = array_filter($collection, function($item) use ($key, $value) {
            return $item[$key] === $value;
        });
        if (count($collection)) {
            $values = array_values($collection);
            return array_shift($values);
        }
        return null;
    }

    public function find($type, $id)
    {
        return $this->findOneByKey($type, 'id', $id);
    }

    public function findOneByName($type, $name)
    {
        switch ($type) {
            case IModxResource::TYPE_CHUNK:
            case IModxResource::TYPE_SNIPPET:
            case IModxResource::TYPE_TEMPLVAR:
                $key = 'name';
                break;
            case IModxResource::TYPE_CONTENT:
                $key = 'alias';
                break;
            case IModxResource::TYPE_TEMPLATE:
                $key = 'templatename';
                break;
            default:
                $key = 'name';
        }
        return $this->findOneByKey($type, $key, $name);
    }
}