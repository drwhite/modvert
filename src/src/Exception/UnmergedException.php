<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 27/09/15
 * Time: 22:01
 */

namespace Qst\Exception;


class UnmergedException extends \Exception
{
    protected $message = 'Есть нерешенные конфликты или они не загружены в DB';
}