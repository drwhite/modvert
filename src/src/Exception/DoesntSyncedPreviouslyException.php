<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 20.09.15
 * Time: 23:22
 */

namespace Qst\Exception;


class DoesntSyncedPreviouslyException extends \Exception
{

    protected $message = 'Ошибка обновления. Текущая ветка никогда ранее не была синхронизована.';

}