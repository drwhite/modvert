<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 27/09/15
 * Time: 22:17
 */

namespace Qst\Command;

use Illuminate\Database\Capsule\Manager as DB;
use Qst\App;
use Qst\Exception\DoesntSyncedPreviouslyException;
use Qst\Log;
use Qst\Model\Category;
use Qst\Model\TV;
use Qst\ResourceManager;
use Qst\ResourceModel;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use PHPGit\Exception\GitException;
use Qst\Driver\DatabaseDriver;
use Qst\Driver\RemoteDriver;
use Qst\IModxResource;
use Qst\Model\Chunk;
use Qst\Model\Snippet;
use Qst\Model\Content;
use Qst\Model\Template;

class Command
{

    const BRANCH_LOCAL = 'modvert_changes_local';

    const BRANCH_REMOTE = 'modvert_changes_remote';

    protected static $remote_url;

    protected static $tables_map = [
        IModxResource::TYPE_CHUNK => 'modx_site_htmlsnippets',
        IModxResource::TYPE_SNIPPET => 'modx_site_snippets',
        IModxResource::TYPE_CONTENT => 'modx_site_content',
        IModxResource::TYPE_TEMPLATE => 'modx_site_templates',
    ];

    /**
     * @var \PHPGit\Git
     */
    protected static $repo;

    protected static $current_branch;

    protected static $current_revision;

    protected static $input;
    protected static $output;

    /**
     * [$resourceManager description]
     * @var ResourceManager
     */
    protected static $resourceManager;

    /**
     * [$rm description]
     * @var ResourceManager
     */
    protected static $rm;

    protected static $affect = [];

    public static function init($args=[])
    {
        self::prepareRepo();
        self::$input = new ArrayInput([]);
        self::$output = new ConsoleOutput();
        self::$resourceManager = new ResourceManager();

        $stages = App::config('stages');
        if (!array_key_exists('stage', $args) || !array_key_exists($args['stage'], $stages)) {
            $args['stage'] = App::config('default_stage');
        }
        self::$remote_url = App::config('stages')[$args['stage']]['remote_url'];
    }

    public static function parseOptions($args)
    {
        $opts = $args;
        if (count($opts)) {
            foreach ($args as $key => $value) {
                if (preg_match('/\-\-force\-rewrite\=(local|remote)/i', $value, $matches)) {
                    if ($matches && count($matches)) {
                        $opts['force_rewrite'] = $matches[1];
                    }
                }
                if (preg_match('/\-\-stage\=(test|staging|development)/i', $value, $matches)) {
                    if ($matches && count($matches)) {
                        $opts['stage'] = $matches[1];
                    }
                }
            }
            if (!array_key_exists('stage', $opts)) {
                $opts['stage'] = App::config('default_stage');
            }
        } else {
            $opts = ['stage' => App::config('default_stage')];
        }
        return $opts;
    }

    protected static function hasUnmergedRevision()
    {
        $revision = self::getLastRevision();
        return (isset($revision) && $revision && $revision['conflict']);
    }

    protected static function getLastRevision()
    {
        return DB::table('modvert_history')
            ->where('branch', '=', self::$current_branch)
            //->where('revision', '=', self::$current_branch)
            ->orderBy('id', 'desc')
            ->limit(1)
            ->first();
    }

    public static function getTableNameByType($type)
    {
        return self::$tables_map[$type];
    }

    /**
     * [commit description]
     * @return bool [description]
     */
    protected static function commit()
    {

        // Ищем фиксацию с текущей ревизией в истории
        $record = DB::table('modvert_history')
            ->where(['revision' => self::$current_revision])
            ->first();
        if (!$record) {
            DB::table('modvert_history')->insert([
                'branch' => self::$current_branch,
                'revision' => self::$current_revision
            ]);
        } else {
            DB::table('modvert_history')->where('id', '=', $record['id'])->update(['conflict' => 0]);
            App::console()->writeln('<question>Ревизия ранее была зафиксирована. Обновляю статус.</question>');
            return true;
        }
        return true;
    }

    public static function setAffect($affect)
    {
        if (!is_array($affect)) {
            throw new \InvalidArgumentException('Параметр affect должен иметь тип array');
        }
        self::$affect = $affect;
    }

    protected static function prepareRepo()
    {
        self::$repo = new \PHPGit\Git();
        self::$repo->setRepository(App::make('app_path'));
        // Retrive the current branch
        $br = array_filter(self::$repo->branch(), function ($branch) {
            return $branch['current'];
        });
        self::$current_branch = array_keys($br)[0];
        self::$current_revision = self::$repo->log()[0]['hash'];
    }

    protected static function getCurrentRevision()
    {
        return self::$repo->log()[0]['hash'];
    }

    /**
     * Git Diff from HEAD to to_commit
     * @param $to_commit
     * @return array|null
     */
    protected static function gitDiff($to_commit, $from=null)
    {
        $output = null;
        if (!$from) $from = self::$current_revision;
        App::console()->writeln('<comment>git diff --name-only ' . $to_commit . ' ' . self::$current_revision . ' ' . App::config('storage') . '</comment>');
        exec('git diff --name-only ' . $to_commit . ' ' . $from . ' ' . App::config('storage'), $output);
        Log::debug('Raw ' . var_export($output, 1));
        $output = array_map(function ($item) {
            $item = preg_replace_callback('/\\\\[0-7]{3}/', function ($matches) {
                return chr(octdec($matches[0]));
            }, $item);
            return str_replace('"', '', $item);
        }, $output);
        Log::debug('Processed ' . var_export($output, 1));
        return $output;
    }

    protected static function checkoutB($branch, $ancestor = null)
    {
        if (!$ancestor) {
            $revision = self::getLastRevision();
            if ($revision) {
                $ancestor = $revision['revision'];
            } else {
                return false;
            }
        }
        $exists = array_filter(self::$repo->branch(), function ($b) use ($branch) {
            return $branch == $b['name'];
        });
        if ($exists && count($exists)) {
            // self::$repo->checkout($branch);
            self::$repo->branch->delete($branch, ['force' => true]);
        }
        self::$repo->checkout->create($branch, $ancestor);
    }

    /**
     * Проверяет есть ли изменения в индексе GIT
     *
     * @return bool
     */
    protected static function hasUnstagedChanges()
    {
        $changes = self::$repo->status()['changes'];
        if (count($changes)) {
            $changes = array_filter($changes, function ($item) {
                return $item['index'] !== '?'; // исключаем Untracked files
            });
        }
        return count($changes) > 0;
    }

    protected static function getUnstagedChanges()
    {
        $changes = self::$repo->status()['changes'];
        if (count($changes)) {
            $changes = array_filter($changes, function ($item) {
                return $item['index'] !== '?'; // исключаем Untracked files
            });
        }
        return $changes;
    }

    /**
     * Synonim for the hasUnmergedRevision method
     * @return boolean [description]
     */
    protected static function hasPreviouslyUnresolvedConflict()
    {
        return self::hasUnmergedRevision();
    }

    protected static function ask($question_text, $default = false)
    {
        //self::init();
        $question = new ConfirmationQuestion($question_text . ' (yes|no) [' . ($default ? 'yes' : 'no') . ']: ', $default);
        $r = App::consoleDialog()->ask(self::$input, self::$output, $question);
        return $r;
    }

    public static function updateRemoteDB($files)
    {
        App::console()->writeln('<comment>Обновляю файлы:</comment>');

        $resources = array_map(function ($file) {
            App::console()->writeln('<comment>' . $file . '</comment>');
            $resource = ResourceModel::getResource(App::make('app_path') . $file);
            return [
                "tablename" => $resource->getTableName(),
                "type" => $resource->getType(),
                "data" => $resource->toArray()
            ];
        }, $files);

        self::$resourceManager->setDriver(new \Qst\Driver\RemoteDriver(self::$remote_url));
        return self::$resourceManager->update($resources);
    }

    /**
     * Возвращает различие между файлами и базой данных
     * @return array
     */
    protected static function getDBDiff()
    {
        $branch = self::BRANCH_LOCAL;
        $changes = [
            IModxResource::TYPE_CHUNK => [],
            IModxResource::TYPE_SNIPPET => [],
            IModxResource::TYPE_CONTENT => [],
            IModxResource::TYPE_TEMPLATE => [],
            IModxResource::TYPE_CATEGORY => [],
        ];
        $categories = self::getResourceCollection($branch, IModxResource::TYPE_CATEGORY);
        foreach ($categories as $category) {
            $model = new Category($category);
            if ($model->isChanged()) {
                $changes[$model->getType()][] = $model;
            }
        }

        $chunks = self::getResourceCollection($branch, IModxResource::TYPE_CHUNK);
        foreach ($chunks as $chunk) {
            $model = new Chunk($chunk);
            if ($model->isChanged()) {
//                App::console()->writeln('<fg=blue>чанк ' . $model->getName() . ' изменен </>');
                $changes[$model->getType()][] = $model;
            }
        }
        $snippets = self::getResourceCollection($branch, IModxResource::TYPE_SNIPPET);
        foreach ($snippets as $snippet) {
            $model = new Snippet($snippet);
            if ($model->isChanged()) {
//                App::console()->writeln('<fg=magenta>сниппет ' . $model->getName() . ' изменен </>');
                $changes[$model->getType()][] = $model;
            }
        }
        $content = self::getResourceCollection($branch, IModxResource::TYPE_CONTENT);
        foreach ($content as $page) {
            $model = new Content($page);
            if ($model->isChanged()) {
//                App::console()->writeln('<fg=white>страница ' . $model->getName() . ' изменена </>');
                $changes[$model->getType()][] = $model;
            }
        }
        $templates = self::getResourceCollection($branch, IModxResource::TYPE_TEMPLATE);
        foreach ($templates as $template) {
            $model = new Template($template);
            if ($model->isChanged()) {
//                App::console()->writeln('<fg=cyan>шаблон ' . $model->getName() . ' изменен </>');
                $changes[$model->getType()][] = $model;
            }
        }
        return $changes;
    }

    /**
     * Update DB based on a .model files
     * @param string $branch ветка (self::BRANCH_LOCAL || self::BRANCH_REMOTE)
     * @param bool $force Если установлен, то ресурс будет перезаписан без проверки на различия
     * @return bool
     */
    protected static function update($branch, $force=false)
    {

        $categories = self::getResourceCollection($branch, IModxResource::TYPE_CATEGORY);
        foreach ($categories as $category) {
            $model = new Category($category);
            if ($model->isChanged() || $force) {
                App::console()->writeln('<fg=yellow>' . ($branch === self::BRANCH_LOCAL ? 'локальная ' : 'удаленная ') . 'категория ' . $model->getName() . ' изменена </>');
                $model->serialize();
            }
        }

        $chunks = self::getResourceCollection($branch, IModxResource::TYPE_CHUNK);
        foreach ($chunks as $chunk) {
            $model = new Chunk($chunk);
            if ($model->isChanged() || $force) {
                App::console()->writeln('<fg=blue>' . ($branch === self::BRANCH_LOCAL ? 'локальный ' : 'удаленный ') . 'чанк ' . $model->getName() . ' изменен </>');
                $model->serialize();
            }
        }
        $snippets = self::getResourceCollection($branch, IModxResource::TYPE_SNIPPET);
        foreach ($snippets as $snippet) {
            $model = new Snippet($snippet);
            if ($model->isChanged() || $force) {
                App::console()->writeln('<fg=magenta>' . ($branch === self::BRANCH_LOCAL ? 'локальный ' : 'удаленный ') . 'сниппет ' . $model->getName() . ' изменен </>');
                $model->serialize();
            }
        }
        $content = self::getResourceCollection($branch, IModxResource::TYPE_CONTENT);
        foreach ($content as $page) {
            $model = new Content($page);
            if ($model->isChanged() || $force) {
                App::console()->writeln('<fg=white>' . ($branch === self::BRANCH_LOCAL ? 'локальная ' : 'удаленная ') . 'страница ' . $model->getName() . ' изменена </>');
                $model->serialize();
            }
        }
        $templates = self::getResourceCollection($branch, IModxResource::TYPE_TEMPLATE);
        foreach ($templates as $template) {
            $model = new Template($template);
            if ($model->isChanged() || $force) {
                App::console()->writeln('<fg=cyan>' . ($branch === self::BRANCH_LOCAL ? 'локальный ' : 'удаленный ') . 'шаблон ' . $model->getName() . ' изменен </>');
                $model->serialize();
            }
        }
        $tvs = self::getResourceCollection($branch, IModxResource::TYPE_TEMPLVAR);
        foreach ($tvs as $tv) {
            $model = new TV($tv);
            if ($model->isChanged() || $force) {
                App::console()->writeln('<fg=blue>' . ($branch === self::BRANCH_LOCAL ? 'локальный ' : 'удаленный ') . 'параметр ' . $model->getName() . ' изменен </>');
                $model->serialize();
            }
        }
        return true;
    }

    protected static function dumpLocal()
    {
        self::update(self::BRANCH_LOCAL, true);
    }

    /**
     * Обновление локальных ресурсов
     *
     * Обновляем файлы на основе локальной БД
     * Предупреждаем о том, что файлы перетрутся, если будет найдено различие между файловой версией и БД
     * @param bool $commit
     * @return bool
     */
    protected static function updateLocal($commit=true)
    {
        /**
         * @Todo Проверить состояние файлов (сравнить с версией в БД). Если есть различия, то вывести их в консоль и спросить
         */
        $changes = self::getDBDiff();
        $hasChanges = false;
        foreach ($changes as $type => $ch) {
            if (count($ch)) {
                $hasChanges = true;
                break;
            }
        }
        $hasChanges |= self::hasUnstoredModels();
        if ($hasChanges) { // Файлы и БД разнятся
            foreach ($changes as $type => $ch) {
                if (count($ch)) {
                    App::console()->writeln("<fg=magenta>$type:</>");
                    /** @var ResourceModel $model */
                    foreach ($ch as $model) {
                        App::console()->writeln("<fg=blue>  -- " . $model->getBoundFile() . ":</>");
                    }
                }
            }
            $result = self::ask("Файлы моделей перетрутся содержимым ресурсов из БД. \n" .
                "Если Вы делали правки в файлах моделей, то выполните команду `modvert load` для загрузки изменений в БД\n" .
                "Уверены, что хотите продолжить?"
            , false);var_dump(self::$remote_url);
            if ($result) {
                // @Todo Тут надо обновить файлы из БД
                self::update(self::BRANCH_LOCAL);
                if ($commit && self::hasUnstagedChanges()) { // Если файлы обновились, то фиксируем их
                    self::$repo->add(App::config('storage') . '*');
                    self::$repo->commit('local changes fix');
                }
            }
            return $result;
        }
        return true;
    }

    /**
     * Возвращает TRUE, если текущая ветка ранее была синхронизована
     * Иначе FALSE
     * @return bool
     */
    protected static function branchWasSynced()
    {
        $revision = DB::table('modvert_history')
            ->where('branch', '=', self::$current_branch)
            //->where('revision', '=', self::$current_branch)
            ->orderBy('created', 'desc')
            ->limit(1)
            ->first(['revision']);
        $rev = $revision['revision'];
        if (!$revision || !$rev) {
            return false;
        }
        return true;
    }

    /**
     * Возвращает изменения в индексе по сравнению с коммитом последней синхронизации
     *
     * -1 в случае, если
     * @return array|int
     * @throws DoesntSyncedPreviouslyException
     */
    protected static function getChangesUpToLastSync()
    {
        $revision = DB::table('modvert_history')
            ->where('branch', '=', self::$current_branch)
            //->where('revision', '=', self::$current_branch)
            ->orderBy('created', 'desc')
            ->limit(1)
            ->first(['revision']);
        $rev = $revision['revision'];
        if (!$revision || !$rev) {
            throw new DoesntSyncedPreviouslyException();
        }
        App::console()->writeln('<comment>Branch: ' . self::$current_branch);
        App::console()->writeln('<comment>Last update revision: ' . $rev);
        App::console()->writeln('<comment>Current revision: ' . self::$current_revision . '</comment>');
        $diff = self::gitDiff($rev);
        if (!$diff || !count($diff)) return false;
        return $diff;
    }

    /**
     * Если текущая ревизия была синхронизована, возвращает TRUE, иначе FALSE
     * @return boolean
     */
    protected static function currentRevisionSynced()
    {
        self::$current_revision = self::$repo->log()[0]['hash'];
        // Ищем фиксацию с текущей ревизией в истории
        $record = DB::table('modvert_history')
            ->where('revision', '=', self::$current_revision)
            ->first();
        return (isset($record) && $record && $record['revision']);
    }

    protected static function setResolved($revision)
    {
        App::console()->writeln('<comment>setResolved("' . $revision . '")</comment>');
        return DB::table('modvert_history')
            ->where('revision', '=', $revision)
            ->update(['conflict'=>0]);
    }

    /**
     * @param $branch
     * @param $type
     * @return array
     */
    public static function getResourceCollection($branch, $type)
    {
        if (!self::$rm) {
            self::$rm = new ResourceManager();
        }
        if ($branch === self::BRANCH_LOCAL) {
            self::$rm->setDriver(new DatabaseDriver());
        } else {
            self::$rm->setDriver(new RemoteDriver(self::$remote_url));
        }
        return self::filterAffect(self::$rm->get($type), $type);
    }

    /**
     * Удаление временных веток
     */
    protected static function cleanup()
    {
        // Пробуем удалить старые ветки
        try {
            App::console()->writeln('<info>Удаляю ветку "' . 'modvert/' . self::$current_branch . '__' . self::BRANCH_REMOTE . '" ...</info>');
            self::$repo->branch->delete('modvert/' . self::$current_branch . '__' . self::BRANCH_REMOTE, ['force' => true]);
        } catch (GitException $ex) {
            App::console()->writeln('<error>Не удалось удалить старые ветки. Возможно их еще не сущесвтует.</error>');
        }
    }


    /**
     * Отфильтровывает только те ресурсы, которые переданы в качестве параметра affect
     *
     * @param $collection
     * @param $type
     * @return array [type]             [description]
     */
    protected static function filterAffect($collection, $type)
    {
        if (count(self::$affect)) {
            $allowed_types = array_keys(self::$affect);
            if (in_array($type, $allowed_types)) {
                $allowed_resource_names = self::$affect[$type];
                $collection = array_filter($collection, function ($item) use ($allowed_resource_names, $type) {
                    switch ($type) {
                        case IModxResource::TYPE_CHUNK:
                        case IModxResource::TYPE_SNIPPET:
                            $name = $item['name'];
                            break;
                        case IModxResource::TYPE_CONTENT:
                            $name = $item['alias'];
                            break;
                        case IModxResource::TYPE_TEMPLATE:
                            $name = $item['templatename'];
                            break;
                        default:
                            $name = $item['name'];
                            break;
                    }
                    return in_array($name, $allowed_resource_names);
                });
            }
        }
        return $collection;
    }

    /**
     * Возвращает TRUE, если есть модели, незафиксированные в БД
     */
    protected static function hasUnstoredModels()
    {
        $types = [
            IModxResource::TYPE_CHUNK,
            IModxResource::TYPE_SNIPPET,
            IModxResource::TYPE_CONTENT,
            IModxResource::TYPE_TEMPLATE,
            IModxResource::TYPE_TEMPLVAR,
        ];
        $hasUnstored = false;
        foreach ($types as $type) {
            if (count(self::$resourceManager->getUnstoredResources($type))) {
                $hasUnstored = true;
                break;
            }
        }
        return $hasUnstored;
    }
}