<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 30/09/15
 * Time: 15:26
 */

namespace Qst\Command;


use Qst\App;

class Install extends Command
{

    public static function run($args=null)
    {
        $tableExists = App::db()
            ->select("show tables LIKE 'modvert_history'");
        if (count($tableExists)) {
            App::console()->writeln('<info>ModVert уже установлен.</info>');
        } else {
            $createSQL = <<<'SQL'
CREATE TABLE `modvert_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch` varchar(255) DEFAULT NULL,
  `revision` varchar(255) DEFAULT NULL,
  `conflict` tinyint(4) DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=120 DEFAULT CHARSET=utf8;
SQL;
            $r = App::db()->insert($createSQL);
            if ($r) {
                App::console()->writeln('<info>ModVert успешно установлен.</info>');
            } else {
                App::console()->writeln('<error>Ошибка установки.</error>');
            }
        }
//        if (!file_exists(App::make('app_path') . 'modvert.yml')) {
//            file_put_contents(App::make('app_path') . 'modvert.yml',
//                file_get_contents(__DIR__ . '/../../../modvert.yml'));
//        }
    }

}