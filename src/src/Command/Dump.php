<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 04/10/15
 * Time: 02:00
 */

namespace Qst\Command;


class Dump extends Command
{

    public static function run($args=[])
    {
        self::init($args);
        $r = self::ask('Вы уверены, что хотите обновить файлы моделей содержимым из локальной БД?');
        if ($r) {
            self::dumpLocal(); // обновить, но не фиксировать
        }
    }

}