<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 02/10/15
 * Time: 22:00
 */

namespace Qst\Command;


use Qst\App;
use Qst\Driver\DatabaseDriver;
use Qst\Driver\RemoteDriver;
use Qst\IModxResource;
use Qst\ResourceModel;

class FixDuplicates extends Command
{

    public static function getKeyElement($key, $element)
    {
        if (is_array($key)) {
            $key = array_map(function($e) use ($element){
                return $element[$e];
            }, $key);
            $key = implode('_', $key);
        } else {
            $key = $element[$key];
        }
        return $key;
    }

    public static function groupByName($collection, $key='name')
    {
        $r = [];
        foreach ($collection as $element) {
            $ek = self::getKeyElement($key, $element);
            if (empty($ek)) dd($element, $ek);
            if (!array_key_exists($ek, $r)) {
                $r[$ek] = [$element['id']];
            } else {
                $r[$ek][] = $element['id'];
            }
        }
        return $r;
    }

    public static function unify($branch, $type, $key, array $ids)
    {
        if (count($ids) >= 2) {
            $actual = max($ids);
            App::console()->writeln("<info>$key актуальна: $actual</info>");
            $another = array_filter($ids, function($e) use ($actual){
               return $e !== $actual;
            });
            foreach ($another as $id) {
                self::deleteElement($branch, $type, $id);
            }
        }
    }

    protected static function deleteElement($branch, $type, $id)
    {
        self::$resourceManager->setDriver(
            $branch===self::BRANCH_LOCAL
                ? new DatabaseDriver()
                : new RemoteDriver(self::$remote_url)
        );
        self::$resourceManager->delete($type, $id);
    }

    private static function _run($remote=false)
    {
        $branch = $remote ? self::BRANCH_REMOTE : self::BRANCH_LOCAL;
        $collection = self::getResourceCollection($branch, IModxResource::TYPE_CHUNK);
        $r = self::groupByName($collection);
        foreach ($r as $key=>$ids) {
            self::unify($branch, IModxResource::TYPE_CHUNK, $key, $ids);
        }

        $collection = self::getResourceCollection($branch, IModxResource::TYPE_SNIPPET);
        $r = self::groupByName($collection);
        foreach ($r as $key=>$ids) {
            self::unify($branch, IModxResource::TYPE_SNIPPET, $key, $ids);
        }

        $collection = self::getResourceCollection($branch, IModxResource::TYPE_CONTENT);
        $r = self::groupByName($collection, ['parent', 'alias']);
        foreach ($r as $key=>$ids) {
            self::unify($branch, IModxResource::TYPE_CONTENT, $key, $ids);
        }

        $collection = self::getResourceCollection($branch, IModxResource::TYPE_TEMPLATE);
        $r = self::groupByName($collection, 'templatename');
        foreach ($r as $key=>$ids) {
            self::unify($branch, IModxResource::TYPE_TEMPLATE, $key, $ids);
        }
    }

    private static function runLocal()
    {
        self::_run();
    }

    private static function runRemote()
    {
        self::_run(true);
    }

    public static function run($args)
    {
        $args = self::parseOptions($args);
        self::init($args);
        self::runLocal();
        self::runRemote();
    }

}