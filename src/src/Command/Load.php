<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 14.09.15
 * Time: 23:06
 */

namespace Qst\Command;

use Qst\App;
use Qst\Model\Category;
use Qst\Model\Chunk;
use Qst\Model\Snippet;
use Qst\Model\Content;
use Qst\Model\Template;
use Illuminate\Database\Capsule\Manager as DB;
use Qst\Model\TV;

class Load extends Command
{
    /**
     *
     */
    public static function run($args=null)
    {
        self::prepareRepo();
        //$rev = self::getLastRevision();
        if (file_exists(App::config('storage') .'category')) {
            App::console()->writeln('<question>Загрузка категорий</question>');
            $files = scandir(App::config('storage') .'category');
            foreach ($files as $file) {
                if (!in_array($file, ['.', '..'])) {
                    $filename = App::config('storage') .'category/' . $file;
                    $tv = new Category();
                    $tv->deserialize($filename);
                }
            }
        }
        if (file_exists(App::config('storage') .'chunk')) {
            $files = scandir(App::config('storage') .'chunk');
            App::console()->writeln('<question>Загрузка чанков</question>');
            foreach ($files as $file) {
                if (!in_array($file, ['.', '..'])) {
                    $filename = App::config('storage') .'chunk/' . $file;
//                App::console()->writeln("<comment>  + ${file}</comment>");
                    $chunk = new Chunk();
                    $chunk->deserialize($filename);
                }
            }
        }
        if (file_exists(App::config('storage') .'snippet')) {
            $files = scandir(App::config('storage') . 'snippet');
            App::console()->writeln('<question>Загрузка сниппетов</question>');
            foreach ($files as $file) {
                if (!in_array($file, ['.', '..'])) {
                    $filename = App::config('storage') . 'snippet/' . $file;
                    //                App::console()->writeln("<comment>  + ${file}</comment>");
                    $snippet = new Snippet();
                    $snippet->deserialize($filename);
                }
            }
        }
        if (file_exists(App::config('storage') .'content')) {
            App::console()->writeln('<question>Загрузка страниц</question>');
            $files = scandir(App::config('storage') . 'content');
            foreach ($files as $file) {
                if (!in_array($file, ['.', '..'])) {
                    $filename = App::config('storage') . 'content/' . $file;
                    //                App::console()->writeln("<comment>  + ${file}</comment>");
                    $content = new Content();
                    $content->deserialize($filename);
                }
            }
        }
        if (file_exists(App::config('storage') .'template')) {
            App::console()->writeln('<question>Загрузка шаблонов</question>');
            $files = scandir(App::config('storage') . 'template');
            foreach ($files as $file) {
                if (!in_array($file, ['.', '..'])) {
                    $filename = App::config('storage') . 'template/' . $file;
                    //                App::console()->writeln("<comment>  + ${file}</comment>");
                    $template = new Template();
                    $template->deserialize($filename);
                }
            }
        }
        if (file_exists(App::config('storage') .'tv')) {
            App::console()->writeln('<question>Загрузка переменных шаблонов</question>');
            $files = scandir(App::config('storage') . 'tv');
            foreach ($files as $file) {
                if (!in_array($file, ['.', '..'])) {
                    $filename = App::config('storage') . 'tv/' . $file;
                    //                App::console()->writeln("<comment>  + ${file}</comment>");
                    $tv = new TV();
                    $tv->deserialize($filename);
                }
            }
        }
    }

}