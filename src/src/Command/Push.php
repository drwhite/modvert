<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 03/10/15
 * Time: 18:39
 */

namespace Qst\Command;
use Qst\App;

/**
 * загрузка избенений в удаленную БД
 *
 * Class Push
 * @package Qst\Command
 */
class Push extends Command
{
    /**
     * modvert push --test
     * modvert push --staging
     * @param array $args
     */
    public static function run($args = [])
    {
        self::init($args);
    }

}