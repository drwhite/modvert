<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 14.09.15
 * Time: 23:06
 */

namespace Qst\Command;

use Qst\App;
use Qst\Driver\DatabaseDriver;
use Qst\Driver\RemoteDriver;
use Qst\IModxResource;
use Qst\Model\Chunk;
use Qst\Model\Snippet;
use Qst\Model\Content;
use Qst\Model\Template;
use Qst\ResourceManager;
use Diff_Renderer_Text_Unified;
use Diff;

class Status
{
    /**
     *
     */
    public static function run($args=[])
    {
        if (count($args) == 1) {
            $files = array_map(function($item){
                return App::make('app_path') . $item;
            }, $args);
            $file = $files[0];

            /**
             * Определяем тип ресурса
             */
            preg_match('/system\/modvert\/storage\/(chunk|snippet|template|content)/', $file, $matches);
            if ($matches && count($matches)>1) {
                $type = $matches[1];
                switch($type) {
                    case IModxResource::TYPE_CHUNK:
                        $resource = new Chunk();
                        break;
                    case IModxResource::TYPE_SNIPPET:
                        $resource = new Snippet();
                        break;
                    case IModxResource::TYPE_CONTENT:
                        $resource = new Content();
                        break;
                    case IModxResource::TYPE_TEMPLATE:
                        $resource = new Template();
                        break;
                }
                $resource->loadFromFile($file);
                $dumped_version = $resource->toArray();
                $rm = new ResourceManager();
                $rm->setDriver(new DatabaseDriver());
                $db_version = $rm->find($type, $dumped_version['id']);
                if (in_array('description', array_keys($db_version)) && in_array('description', array_keys($dumped_version))) {
                    unset($db_version['description']);
                    unset($dumped_version['description']);
                }
                $diff = array_diff_assoc($db_version, $dumped_version);
                if (count($diff)) {
                    App::console()->writeln('<question>Resource has changes</question>');
                    $diff_keys = array_keys($diff);
                    if (in_array('snippet', $diff_keys) || in_array('content', $diff_keys)) {
                        $diff_key = $diff_keys[0];
                        $a = explode(PHP_EOL, $dumped_version[$diff_key]);
                        $b = explode(PHP_EOL, $db_version[$diff_key]);
                        $diff = new Diff($a, $b, [
                            'ignoreWhitespace' => true,
                            'ignoreCase' => true,
                        ]);
                        $renderer = new \Diff_Renderer_Text_Unified();
                        App::console()->writeln('<info>' . $diff->render($renderer) . '</info>');
                    }
                }
            } else {
                App::console()->writeln('<error>Путь к файлу ресурса указан неверно</error>');
                exit;
            }
        } else {
            App::console()->writeln('<error>Должен быть передан лишь один аргумент</error>');
            exit;
        }
    }

}