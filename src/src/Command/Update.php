<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 14.09.15
 * Time: 23:06
 */

namespace Qst\Command;

use Qst\App;
use Qst\Exception\DoesntSyncedPreviouslyException;

/**
 * 1. Фиксируем локальные изменения.
 * 2. Фиксируем удаленные изменения.
 * 3. Объединяем локальные и удаленные
 * 4. Если были локальные изменения
 *
 * Class Update
 * @package Qst\Command
 */
class Update extends Fetch
{
    /**
     *
     */
    public static function run($args = null)
    {
        $args = self::parseOptions($args);
        self::init($args);
        self::$current_revision = self::$repo->log()[0]['hash'];
        // if (self::currentRevisionSynced()) {
        //     App::console()->writeln('<question>Ревизия уже зафиксирована</question>');
        //     return false;
        // }
        if (!self::branchWasSynced()){
            App::console()->writeln('<info>Текущая ветка никогда ранее не синхронизировалась.</info>');
            App::console()->writeln('<info>Пробую зафиксировать начальную ревизию.</info>');
            self::commit();
        }
        if (!parent::run($args)) return false;
        self::$current_revision = self::$repo->log()[0]['hash'];
        try {
            if ($diff = self::getChangesUpToLastSync()) { // expect throw DoesntSyncedPreviouslyException
                App::console()->writeln('<info>Найдены незафиксированные изменения</info>');
                App::console()->writeln('<question>Фиксирую</question>');
                // Вот тут надо отправить изменения на удаленный сервер!
                if ($updated = self::updateRemoteDB($diff)) {
                    if (self::commit()) { // записываю хеш текущего коммита в таблицу modvert_history
                        App::console()->writeln('<info>Удаленный сервер обновлен успешно</info>');
                    } else {
                        App::console()->writeln('<error>Не удалось зафиксировать изменения</error>');
                    }
                }
            } else {
                App::console()->writeln('<error>Новых обновлений нет.</error>');
            }
        } catch (DoesntSyncedPreviouslyException $ex) {
            App::console()->writeln('<info>Текущая ветка никогда ранее не синхронизировалась.</info>');
            App::console()->writeln('<info>Пробую зафиксировать ревизию и повторить обновление.</info>');
            self::commit();
            return self::run();
        } catch (\Exception $ex) {
            App::console()->writeln($ex->getTraceAsString());
            App::console()->writeln('<error>Произошла непредвиденная ошибка :-(</error>');
            return false;
        }
        return true;
    }
}