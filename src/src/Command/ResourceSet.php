<?php
/**
 * Created by PhpStorm.
 * User: Jasper
 * Date: 10/7/2015
 * Time: 9:20 PM
 */

namespace Qst\Command;

use Qst\App;

class ResourceSet extends Command
{

    public static function run($args=[])
    {
        $request = App::request();
        $response = App::response();
        $rm = new \Qst\ResourceManager();
        $rm->setDriver(new \Qst\Driver\DatabaseDriver());

        if ($request->isMethod('post')) {
            $data = $request->get('data');
            $rm->update($data);
            $response->setContent(json_encode(['status'=>'OK']));
        } elseif ($request->isMethod('delete')) {
            $data = $request->get('data');
            $rm->delete($data['type'], $data['id']);
        } else {
            $response->setContent(json_encode(['status'=>'FAIL', 'message'=>'Method doesn\'t allowed']));
        }

        $response->send();
        die();
    }

}