<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 14.09.15
 * Time: 23:06
 */

namespace Qst\Command;

use PHPGit\Exception\GitException;
use Illuminate\Database\Capsule\Manager as DB;
use Qst\App;
use Qst\Exception\ModvertMergeConflictException;
use Qst\Exception\UnmergedException;
use Qst\IModxResource;


/**
 * Переключается на ветку локальных изменений и сливает дамп из БД в файлы
 * Проверяет были ли изменения файлов в индексе
 * Если были, то ставит метку необходимости слияния
 *
 * То же самое делает для ветки удаленных изменений
 *
 * В результате переключается в основную ветку и делает слияние вспомогательных веток
 * в основную ветку
 *
 */
class Fetch extends Command
{
    /**
     * @param null $args
     * @return bool
     */
    public static function run($args = null)
    {
        self::init($args);
        try {
            if (self::hasUnmergedRevision()) throw new UnmergedException;// Если была выполнена предыдущая комманда, породившая конфликт, то нужно вызвать комманду load
            /**
             * Если указаны конкретные ресурсы
             */
            if (
                isset($args) && is_array($args) && count($args)
            ) { //  только для теста
                if (array_key_exists('affect', $args) && is_array($args['affect'])) {
                    self::$affect = $args['affect'];
                }
            }

            $repo = self::$repo;
            if (!file_exists(App::config('storage'))) {
                mkdir(App::config('storage'), 0777, true);
                file_put_contents(App::config('storage') . '.gitignore', '');
                $repo->add(App::config('storage') . '*');
                $repo->commit('init storage');
            }
            $current_branch = self::$current_branch;
            $need_merge_remote = false;
            if (self::hasUnstagedChanges()) {
//                foreach (self::getUnstagedChanges() as $changes) {
//                    App::console()->writeln('<info>' . $changes['file'] .'</info>');
//                }
                App::console()->writeln("<error>Для начала зафиксируйте изменения в своей локальной копии</error>");
                return false;
            }

            $modvert_local_branch = 'modvert/' . $current_branch . '__' . self::BRANCH_LOCAL;
            $modvert_remote_branch = 'modvert/' . $current_branch . '__' . self::BRANCH_REMOTE;

            // Если на данный момент мы находимся на временных ветках инструмента ModVerT,
            // то нужно сообщить разработчику о том, что произошла ошибка и необходимо переключиться на ветку разработки
            if (in_array($current_branch, [$modvert_local_branch, $modvert_remote_branch])) {
                App::console()->writeln('<error>Что-то изначально пошло не так.\n' .
                    'Вы находитесь на временной ветке.\n' .
                    'Переключитесь на любую из веток вашего цикла разработки</error>'
                );
                return false;
            }

            self::cleanup(); // удаляем временные метки
            $localUpdated = static::updateLocal();
            if (!$localUpdated) {
                App::console()->writeln('<info>Возможно Вам будет полезно знать про дургие комманды:</info>');
                App::printHello();
                return false;
            }

            // Создаем новую временную ветку удаленных изменений и переходим в нее
            App::console()->writeln('<info>Создаю новую временную ветку удаленных изменений и перехожу в нее</info>');
            self::checkoutB($modvert_remote_branch);
            self::update(self::BRANCH_REMOTE);
            if (self::hasUnstagedChanges()) {
                $need_merge_remote = true; // Если были изменения, то надо их замержить в ветку разработки
                $repo->add(App::config('storage') . '*');
                // Фиксируем изменения
                App::console()->writeln('<info>Фиксирую изменения</info>');
                $repo->commit('remote changes fix');
            } else {
                App::console()->writeln('<comment>В удаленной DB изменений нет.</comment>');
            }

            App::console()->writeln('<info>Возвращаюсь на исходную ветку</info>');
            $repo->checkout($current_branch);

            if ($need_merge_remote) {
                App::console()->writeln('<info>Вливаю удаленную ветку</info>');
                try {
                    $repo->merge($modvert_remote_branch);
                } catch (GitException $ex) {
                    App::console()->writeln('<error>Произошла ошибка слияния правок. Воспользуйтесь графическим инструментом Git Mergetool для решения конфликта. Затем выполните команду</error> <question>modvert load</question>');
                    $rev = self::getLastRevision();
                    DB::table('modvert_history')->where('id', '=', $rev['id'])->update(['conflict' => 1]); // Говорим, что последняя синхронизация породила конфликт

                    throw new ModvertMergeConflictException();
                }
            }

            if (!self::hasUnstagedChanges()) {
                self::cleanup();
                App::console()->writeln('<question>Все круто.</question>');
                return true;
            } else {
                $rev = self::getLastRevision();
                DB::table('modvert_history')->find($rev['id'])->update(['conflict' => 1]); // Говорим, что последняя синхронизация породила конфликт
                throw new ModvertMergeConflictException();
            }
        } catch (UnmergedException $ex) {
            App::console()->writeln("<error>ошибка объединения</error>");
            if (self::hasUnstagedChanges()) {
                App::console()->writeln("<error>{$ex->getMessage()}</error>");
                App::console()->writeln("<error>Предыдущая синхронизация породила конфликты. Решите их и запустите комманду</error> <question>load</question>");
            } else {
                App::console()->writeln('<info>незафиксированных изменений нет</info>');
                $modvert_remote_branch = 'modvert/' . self::$current_branch . '__' . self::BRANCH_REMOTE;
                self::$repo->checkout($modvert_remote_branch);
                self::update(self::BRANCH_REMOTE);
                if (self::hasUnstagedChanges()) {
                    $diff = self::getUnstagedChanges();
                    App::console()->writeln('<info>' . var_export($diff, 1) . '</info>');
                    self::$repo->add(App::config('storage') . '*');
                    // Фиксируем изменения
                    App::console()->writeln('<info>Фиксирую изменения</info>');
                    self::$repo->commit('remote changes fix');
                    self::$repo->checkout(self::$current_branch);
                } else {
                    self::$current_revision = self::getCurrentRevision();
                    self::$repo->checkout(self::$current_branch);
                    $d_files = self::gitDiff($modvert_remote_branch, self::$current_branch);
                    self::cleanup(); // удаляем старую ветку
                    App::console()->writeln('<info>Diff to update: ' . var_export($d_files, 1) . '</info>');
                    self::updateRemoteDB($d_files);
                    self::setResolved(self::getLastRevision()['revision']);
                    if (self::commit()) { // записываю хеш текущего коммита в таблицу modvert_history
                        App::console()->writeln('<info>Удаленный сервер обновлен успешно</info>');
                    } else {
                        App::console()->writeln('<error>Не удалось зафиксировать изменения</error>');
                    }
                }
                self::run($args);
            }
        } catch (ModvertMergeConflictException $ex) {
            App::console()->writeln('<error>Ужас. Нужно решать конфликты.</error>');
        } catch (\Exception $ex) {
            App::console()->writeln("<error>{$ex->getMessage()}</error>");
            App::console()->writeln('<error>Возникла неизвестная ошибка.</error>');
        }
        return false;
    }


}