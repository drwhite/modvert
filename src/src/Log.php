<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 19.09.15
 * Time: 9:41
 */

namespace Qst;


use Monolog\Logger;

class Log
{

    public static function debug($message)
    {
        return self::log($message, Logger::DEBUG);
    }

    public static function info($message)
    {
        return self::log($message, Logger::INFO);
    }

    public static function warn($message)
    {
        return self::log($message, Logger::WARNING);
    }

    public static function error($message)
    {
        return self::log($message, Logger::ERROR);
    }

    protected static function log($message, $level)
    {
        return App::make('log')->log($level, $message);
    }
}