<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 15.09.15
 * Time: 16:44
 */

namespace Qst;


interface IModxResourceDriver
{

    public function getCollection($type);

    public function update(IModxResource $resource);

    public function batchUpdate($resources);

    public function delete($type, $id);

    public function find($type, $id);

    public function findOneByName($type, $name);

}