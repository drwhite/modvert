<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 10/09/15
 * Time: 01:56
 */

namespace Qst;

use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Foundation\Application;
use Qst\Command\Dump;
use Qst\Command\Fetch;
use Qst\Command\FixDuplicates;
use Qst\Command\Install;
use Qst\Command\Load;
use Qst\Command\Pull;
use Qst\Command\Push;
use Qst\Command\ResourceGet;
use Qst\Command\ResourceSet;
use Qst\Command\Status;
use Qst\Command\Update;
use Qst\Command\Clean;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\HelperSet;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Output\ConsoleOutput;
use Twig_Environment;
use Twig_Loader_Filesystem;
use Symfony\Component\HttpFoundation as SHttp;

class App
{

    /**
     * @var Application
     */
    protected static $instance;


    protected static $console;

    /**
     * @var SHttp\Request
     */
    protected static $request;

    /**
     * @var SHttp\Response
     */
    protected static $response;

    /**
     * @var \Noodlehaus\Config
     */
    protected static $conf;

    public static function getAppInstance()
    {
        return self::$instance;
    }

    public static function init(Application $app, \Noodlehaus\Config $conf)
    {
        self::$instance = $app;
        self::$conf = $conf;
        self::$request = self::$instance->make('request');
        self::$response = self::$instance->make('response');
    }

    public static function run(Application $app, \Noodlehaus\Config $conf, $command=null, $args=[])
    {
        self::$instance = $app;
        self::$conf = $conf;
        self::$request = self::$instance->make('request');
        self::$response = self::$instance->make('response');
        self::execute($command, $args);
    }

    public static function request()
    {
        if (!self::$request) self::$request = self::$instance->make('request');
        return self::$request;
    }

    public static function response()
    {
        if (!self::$response)
            self::$response = self::$instance->make('response');
        return self::$response;
    }

    public static function execute($command=null, $args=[])
    {
        if ('web' === $args['context'] && self::$request) {
            $command = self::$request->query->get('cmd');
        } else {
            $args = \Qst\Command\Command::parseOptions($args);
            App::console()->writeln('<info>Stage: ' . $args['stage'] . '</info>');
        }
        if ($command) {
            \Qst\Command\Command::init($args);
            switch ($command) {
                case 'fetch': // from database into files
                    Fetch::run($args);
                    break;
                case 'load': // from files into database
                    Load::run($args);
                    break;
                case 'status':
                    Status::run($args);
                    break;
                case 'dump':
                    Dump::run($args);
                    break;
                case 'update':
                    Update::run($args);
                    break;
                case 'pull':
                    Pull::run($args);
                    break;
                case 'push':
                    Push::run($args);
                    break;
                case 'install':
                    Install::run($args);
                    break;
                case 'unify':
                    FixDuplicates::run($args);
                    break;
                case 'clean':
                    Clean::run($args);
                    break;
                case 'resource.get':
                    ResourceGet::run($args);
                    break;
                case 'resource.set':
                    ResourceSet::run($args);
                    break;
                default:
                    self::printHello();
            }
        } else {
            self::printHello();
        }
    }

    public static function printHello()
    {
        App::console()->writeln('<comment>Доступные команды:</comment>');
        App::console()->writeln('<info>install              </info><comment>- установка.</comment>');
        App::console()->writeln('<info>clean                </info><comment>- очистка истории обновлений.</comment>');
        App::console()->writeln('<info>update               </info><comment>- обновляет локальные файлы моделей, локальную БД, удаленную БД.</comment>');
        App::console()->writeln('<info>fetch                </info><comment>- обновляет локальные файлы моделей</comment>');
        App::console()->writeln('<info>unify                </info><comment>- унифицирует ресурсы. Удаляет дубликаты</comment>');
        App::console()->writeln('<info>load                 </info><comment>- обновляет локальную БД в соответствие с файлами моделей</comment>');
        App::console()->writeln('<info>status path_to_model </info><comment>- показывает какие изменения произошли в файле модели</comment>');
    }

    public static function config($key, $value=null)
    {
        if (null === $value) {
            $val = self::$conf->get($key);
            $result = $val;
            if (is_string($val)) {
                if (0 === strpos($val, '`')) {
                    $val = substr($val, 1, -1);
                    eval('$result = ' . $val . ';');
                }
            }
            return $result;
        }
        self::$conf->set($key, $value);
        return null;
    }

    public static function make($service)
    {
        return self::$instance->make($service);
    }

    public static function bind($abstract, $concrete)
    {
        self::$instance->bind($abstract, $concrete);
    }


    /**
     * @return QuestionHelper
     */
    public static function consoleDialog()
    {
        return new QuestionHelper();
    }

    public static function console()
    {
        if (!self::$console) return new ConsoleOutput();
        self::$console = new ConsoleOutput();
        return self::$console;
    }

    /**
     * @return \Illuminate\Database\Connection
     */
    public static function db($connection='default')
    {
        return DB::connection($connection);
    }


    /**
     * @param $template
     * @param array $params
     * @return string
     */
    public static function render($template, $params=[])
    {
        $twig = new Twig_Environment(
            new Twig_Loader_Filesystem([__DIR__ . '/../templates']),
            ['auto_reload' => true, 'debug'=>true]
        );
        return $twig->render($template, $params);
    }
}