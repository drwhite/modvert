<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 04/10/15
 * Time: 02:23
 */

namespace Qst\Serializer;


use Qst\App;
use Qst\ResourceModel;

class TVSerializer extends Serializer
{
    public function serialize(ResourceModel $object)
    {
        $path = $this->serializedModelPath . $object->getType() . '/' . $object->getName() . '.model';
        if (!file_exists(dirname($path))) mkdir(dirname($path));
        $content = App::render('tv.html.twig', [
            'data' => $object->getStringInfo(),
        ]);
        $written = $this->writeFile($path, $content);
        return $written;
    }

    public function deserialize($path)
    {
        return include($path);
    }
}