<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 16.09.15
 * Time: 0:48
 */

namespace Qst\Serializer;


use Diff;
use Qst\App;
use Qst\Model\Snippet;
use Qst\ResourceModel;

class PHPSerializer extends Serializer
{

    public function serialize(ResourceModel $object)
    {
        $path = $this->serializedModelPath . $object->getType() . '/' . $object->getName() . '.model';
        if (!file_exists(dirname($path))) mkdir(dirname($path));
        $snippet = $object->getContent();
        $content = App::render('php.html.twig', [
            'comment_data' => $object->getStringInfo(),
            'content' => $snippet
        ]);
        $written = $this->writeFile($path, $content);
        return $written;
    }

    public function deserialize($path)
    {
        $source = file_get_contents($path);
        $tokens = token_get_all($source);
        $docblock = $content = '';
        foreach( $tokens as $token ) {
            if (T_DOC_COMMENT == $token[0]) {
                $docblock = $token[1];
                $content = str_replace($docblock, '', $source);
                $content = preg_replace('/\\r\\n/s', "\n", $content);
                $content = preg_replace('/\<\?php(\\n\\n)(.*)/sm', '${2}', $content);
                $content = preg_replace('/(.*)\?\>/sm', '${1}', $content);
//                $content = preg_replace('/^(\\n*)$/sm', '', $content);
                break;
            }
        }
        $docblock = preg_replace('/\/\*\*(.*)\*\//s', '${1}', $docblock);
        $data = eval($docblock);
        $data['content'] = $content;
        return $data;
    }
}