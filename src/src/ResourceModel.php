<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 10/09/15
 * Time: 00:57
 */

namespace Qst;

use Illuminate\Database\Capsule\Manager as DB;
use Qst\Model\Category;
use Qst\Model\Chunk;
use Qst\Model\Content;
use Qst\Model\Snippet;
use Qst\Model\Template;
use Qst\Model\TV;
use Qst\Serializer\Serializer;

abstract class ResourceModel implements IModxResource
{
    protected $tablename;

    protected $type;

    protected $data;

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * ResourceModel constructor.
     * @param array $data
     */
    public function __construct(array $data=null)
    {
        if ($data) $this->loadFromArray($data);
    }

    public function loadFromArray(array $data)
    {
        $this->data = $data;
        switch($this->type) {
            case IModxResource::TYPE_CHUNK:
            case IModxResource::TYPE_SNIPPET:
                $this->data['snippet'] = preg_replace('/\\r\\n/s', "\n", $this->data['snippet']);
                break;
            case IModxResource::TYPE_CONTENT:
            case IModxResource::TYPE_TEMPLATE:
                $this->data['content'] = preg_replace('/\\r\\n/s', "\n", $this->data['content']);
                break;
        }
        
    }

    public function toArray()
    {
        return $this->data;
    }

    public function isChanged()
    {
        return $this->serializer->isChanged($this);
    }

    public function getDiff()
    {
        $this->isChanged();
        return $this->serializer->diff;
    }

    public function getBoundFile()
    {
        return App::config('storage') . $this->getType() . '/' . $this->getName() . '.model';
    }

    public function serialize()
    {
        return $this->serializer->serialize($this);
    }

    public function deserialize($file)
    {
        $this->loadFromFile($file);
        if ($entity = DB::table($this->tablename)->find($this->data['id'])) {
            DB::connection()->update(
                'update ' . $this->tablename . ' SET ' .
                $this->prepareFields() .
                ' WHERE id = ?',
                [$this->data['id']]
            );
        } else {
            DB::table($this->tablename)->insert($this->data);
        }
    }

    public function loadFromFile($file)
    {
        $this->data = $this->serializer->deserialize($file);
        if (in_array($this->type, [IModxResource::TYPE_CHUNK, IModxResource::TYPE_SNIPPET])) {
            $this->data['snippet'] = $this->data['content'];
            $this->data['snippet'] = preg_replace('/\\r\\n/s', "\n", $this->data['snippet']);
            if ($this->type == IModxResource::TYPE_SNIPPET) {
                $s = strrev($this->data['snippet']);
                $this->data['snippet'] = strrev(preg_replace('/^\\n/', '', $s, 1));
            }
            unset($this->data['content']);
        }
    }

    public function prepareFields()
    {
        $fields = [];
        foreach ($this->data as $field=>$value) {
            if ('id' !== $field) {
                if (in_array($field, ['content', 'snippet', 'description'])) {
                    $value = addslashes($value);
                }
                $fields[] = $field . "='" . $value . "'";
            }
        }
        return join(',', $fields);
    }

    public function getType()
    {
        return $this->type;
    }

    public function getTableName()
    {
        return $this->tablename;
    }

    public function getName()
    {
        switch($this->type) {
            case IModxResource::TYPE_CHUNK:
            case IModxResource::TYPE_SNIPPET:
            case IModxResource::TYPE_TEMPLVAR:
                return $this->data['name'];
            case IModxResource::TYPE_CONTENT:
                return $this->data['id'] . '_' . $this->data['alias'];
            case IModxResource::TYPE_TEMPLATE:
                return $this->data['templatename'];
            case IModxResource::TYPE_CATEGORY:
                return $this->data['category'];
            default:
                return $this->type . '__' . $this->data['id'];
        }
    }

    public function getId()
    {
        return $this->data['id'];
    }

    public function getContent()
    {
        switch($this->type) {
            case IModxResource::TYPE_CHUNK:
            case IModxResource::TYPE_SNIPPET:
                return $this->data['snippet'];
            case IModxResource::TYPE_CONTENT:
            case IModxResource::TYPE_TEMPLATE:
                return $this->data['content'];
            default:
                return '';
        }
    }

    protected function specialEscape($str)
    {
        return preg_replace("/(?<!\\\\)'/sm", '\\\'', $str);
    }

    public function getInfo()
    {
        $data = $this->data;
        switch($this->type) {
            case IModxResource::TYPE_CHUNK:
            case IModxResource::TYPE_SNIPPET:
                unset($data['snippet']);
                $data['description'] = $this->specialEscape($data['description']);
                break;
            case IModxResource::TYPE_CONTENT:
                $unavailable_keys = [
                    'pub_date',
                    'unpub_date',
                    'introtext',
                    'richtext',
                    'createdby',
                    'createdon',
                    'editedby',
                    'editedon',
                    'deletedon',
                    'deletedby',
                    'publishedon',
                    'publishedby',
                    'content'
                ];
                $t_data = $data;
                foreach ($t_data as $key => $value) {
                    if (in_array($key, $unavailable_keys)) {
                        unset($data[$key]);
                    }
                }
                break;
            case IModxResource::TYPE_TEMPLATE:
                unset($data['content']);
                break;
            case IModxResource::TYPE_TEMPLVAR:
                $data['default_text'] = addslashes($data['default_text']);
                $data['elements'] = addslashes($data['elements']);
                break;
        }
        return $data;
    }

    protected function simpleArrayToString($arr)
    {
        return '['  . PHP_EOL . implode(', ', $arr) . PHP_EOL . ']';
    }

    protected function assocArrayToString($arr)
    {
        $str_info = '[' . PHP_EOL;
        foreach ($arr as $key => $value) {
            $str_info .= "'$key' => '$value'," . PHP_EOL;
        }
        $str_info .= ']';
        return $str_info;
    }

    public function getStringInfo()
    {
        $info = $this->getInfo();
        $str_info = 'return [' . PHP_EOL;
        foreach ($info as $key => $value) {
            if (is_array($value)) {
                if ('templates' === $key) {
                    $str_info .= "'$key' => " . $this->simpleArrayToString($value) . ',' . PHP_EOL;
                } else {
                    $str_info .= "'$key' => " . $this->assocArrayToString($value) . ',' . PHP_EOL;
                }
            } else {
                $str_info .= "'$key' => '$value'," . PHP_EOL;
            }
        }
        $str_info .= '];';
        return $str_info;
    }

    /**
     * @param $type
     * @param null $data
     * @return null|Chunk|Content|Snippet|Template|TV
     */
    public static function create($type, $data=null)
    {
        switch($type) {
            case IModxResource::TYPE_CHUNK:
                return new Chunk($data);
            case IModxResource::TYPE_SNIPPET:
                return new Snippet($data);
            case IModxResource::TYPE_CONTENT:
                return new Content($data);
            case IModxResource::TYPE_TEMPLATE:
                return new Template($data);
            case IModxResource::TYPE_TEMPLVAR:
                return new TV($data);
            case IModxResource::TYPE_CATEGORY:
                return new Category($data);
            default:
                return null;
        }
    }

    public function lock()
    {

    }

    public function unlock()
    {

    }

    public static function getResource($file)
    {
        if (preg_match('/\/(content|chunk|snippet|template|tv|category)\/.*/', $file, $matches)) {
            $resource = self::create($matches[1]);
            if (!$resource) return null;
            $resource->loadFromFile($file);
            return $resource;
        }
        return null;
    }

}