<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 10/09/15
 * Time: 00:56
 */

namespace Qst\Model;


use Qst\Serializer\HTMLSerializer;
use Qst\ResourceModel;
use Qst\Serializer\Serializer;

class Chunk extends ResourceModel
{

    protected $tablename = 'modx_site_htmlsnippets';

    protected $type = 'chunk';

    /**
     * @var Serializer
     */
    protected $serializer;


    /**
     * @param array|null $data
     */
    public function __construct(array $data=null)
    {
        parent::__construct($data);
        $this->serializer = new HTMLSerializer();
    }

}