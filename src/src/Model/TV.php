<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 04/10/15
 * Time: 02:21
 */

namespace Qst\Model;


use Qst\Driver\DatabaseDriver;
use Qst\ResourceModel;
use Qst\Serializer\Serializer;
use Qst\Serializer\TVSerializer;

class TV extends ResourceModel
{

    protected $tablename = 'modx_site_tmplvars';

    protected $type = 'tv';

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * @param array|null $data
     */
    public function __construct(array $data=null)
    {
        parent::__construct($data);
        $this->serializer = new TVSerializer();
    }

    public function getTemplates()
    {
        return $this->data['templates'];
    }

    public function getContentValues()
    {
        return $this->data['content_values'];
    }

    public function deserialize($file)
    {
        $this->loadFromFile($file);
        $driver = new DatabaseDriver();
        $driver->update($this);
    }
}