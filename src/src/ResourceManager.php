<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 15.09.15
 * Time: 16:43
 */

namespace Qst;


use Qst\Driver\DatabaseDriver;

class ResourceManager
{

    /**
     * @var IModxResourceDriver
     */
    protected $driver;

    /**
     * @param IModxResourceDriver $driver
     * @return $this
     */
    public function setDriver(IModxResourceDriver $driver)
    {
        $this->driver = $driver;
        return $this;
    }

    /**
     * @param $type
     * @return mixed
     */
    public function get($type) {
        return $this->driver->getCollection($type);
    }

    public function find($type, $pk) {
        return $this->driver->find($type, $pk);
    }

    public function findOneByName($type, $name)
    {
        return ResourceModel::create($type, $this->driver->findOneByName($type, $name));
    }

    public function delete($type, $id)
    {
        return $this->driver->delete($type, $id);
    }

    public function update($resource)
    {
        if (is_array($resource)) {
            return $this->driver->batchUpdate($resource);
        } elseif($resource instanceof \Qst\IModxResource) {
            return $this->updateSingleResource($resource);
        } else {
            return false;
        }
    }

    protected function updateSingleResource(\Qst\IModxResource $resource)
    {
        return $this->driver->update($resource);
    }

    public function getUnstoredResources($type)
    {
        $files = scandir(App::config('storage') . $type);
        $unstored = [];
        $driver = new DatabaseDriver();
        foreach ($files as $file) {
            if (!in_array($file, ['.', '..'])) {
                $filename = App::config('storage') . $type .'/' . $file;
                $resource = ResourceModel::create($type);
                $resource->loadFromFile($filename);
                if (!$driver->find($type, $resource->getId())) {
                    $unstored[] = $resource;
                }
            }
        }
        return $unstored;
    }
}