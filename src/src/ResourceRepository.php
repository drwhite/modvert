<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 10/09/15
 * Time: 10:06
 */

namespace Qst;


class ResourceRepository
{

    protected $tablename = '';

    protected $serializedModelPath = '';

    protected $serializer;

    /**
     * ResourceModel constructor.
     * @param string $serializedModelPath
     */
    public function __construct(Serializer $serializer, $serializedModelPath=null)
    {
        if (!$serializedModelPath) {
            $serializedModelPath = App::config('storage');
        }
        $this->serializer = $serializer;
        $this->serializer->setSerializedModelPath($serializedModelPath);
    }

    public function serialize(ResourceModel $resource)
    {
        return $this->serializer->serialize($resource);
    }

}