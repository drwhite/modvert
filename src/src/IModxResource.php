<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 15.09.15
 * Time: 16:53
 */

namespace Qst;


interface IModxResource
{
    const TYPE_CATEGORY = 'category';
    const TYPE_CHUNK = 'chunk';
    const TYPE_SNIPPET = 'snippet';
    const TYPE_CONTENT = 'content';
    const TYPE_TEMPLATE = 'template';
    const TYPE_TEMPLVAR = 'tv';
}