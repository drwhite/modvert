<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 15.09.15
 * Time: 21:32
 */

$autoloadFiles = [
    __DIR__ . '/../vendor/autoload.php',
    __DIR__ . '/../../../autoload.php',
    __DIR__ . '/../../../vendor/autoload.php',
    __DIR__ . '/../../../../autoload.php',
    __DIR__ . '/../../../../vendor/autoload.php'
];

foreach ($autoloadFiles as $autoloadFile) {
    if (file_exists($autoloadFile)) {
        require_once $autoloadFile;
        //break;
    }
}

use Illuminate\Database\Capsule\Manager as DB;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

define('LARAVEL_START', microtime(true));
define('APP_PATH', getcwd() . '/');

$app = new \Illuminate\Foundation\Application(realpath(__DIR__ . '/../'));

$capsule = new DB;
$env = defined('ENV') ? ENV : env('ENV');
try {
    $conf = new \Noodlehaus\Config([APP_PATH . 'modvert.yml']);
} catch (Noodlehaus\Exception\FileNotFoundException $ex) {
    \Qst\App::console()->writeln('<error>Не найден конфигурационный файл modvert.yml</error>');
    exit;
}

if (PHP_SAPI === 'cli') $conf->set('database.name', $conf->get('database.name') . '_sample');

$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => $conf->get('database.host', '127.0.0.1'),
    'port'      => $conf->get('database.port', 3306),
    'database'  => $conf->get('database.name'),
    'username'  => $conf->get('database.user'),
    'password'  => $conf->get('database.password'),
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);

// Set the event dispatcher used by Eloquent models... (optional)

$capsule->setEventDispatcher(new Dispatcher(new Container));

// Make this Capsule instance available globally via static methods... (optional)
$capsule->setAsGlobal();

// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$capsule->bootEloquent();

$app->bind('app_path', function(){ return APP_PATH; });
$app->bind('env', function() use ($env) { return $env; });

$app->bind('log', function () use ($app) {
    // create a log channel
    $log = new Logger('modvert');
    $log->pushHandler(new StreamHandler($app->make('app_path') . 'logs/modvert.warn.log', Logger::WARNING));
    $log->pushHandler(new StreamHandler($app->make('app_path') . 'logs/modvert.info.log', Logger::INFO));
    $log->pushHandler(new StreamHandler($app->make('app_path') . 'logs/modvert.debug.log', Logger::DEBUG));
    $log->pushHandler(new StreamHandler($app->make('app_path') . 'logs/modvert.err.log', Logger::ERROR));
    return $log;
});

$app->boot();

if (PHP_SAPI === 'cli') {
    try {
        $app->make('request') || $app->make('response');
    } catch (\Exception $ex) {
        $app->bind('request', function() { return null; });
        $app->bind('response', function() { return null; });
    }
}
