<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 15.09.15
 * Time: 17:21
 */
use \Symfony\Component\HttpFoundation as SHttp;
$host = $_SERVER['HTTP_HOST'];

preg_match('/(test|staging)\.questoria\.(loc|ru|com|com\.ua|kz|by|net)/', $host, $hdata);

if (count($hdata) !== 3) {
    die('Этот скрипт не может быть запущен на рабочем сайте!');
}
$subhost = $hdata[1];
$domain = $hdata[2];
if ($subhost === 'test') {
    if ($domain === 'loc') {
        $env = 'testing';
    } else {
        $env = 'staging';
    }
} elseif ($subhost === 'staging') {
    $env = 'production';
} else {
    die('Этот скрипт не может быть запущен на неизвестном сайте!');
}

define('ENV', $env);
include __DIR__ . '/../bootstrap.php';

\Qst\App::run($app, $conf);

// header('Content-Type: application/json; charset=utf-8');

// 

$request = SHttp\Request::createFromGlobals();
$response = new SHttp\Response(
    'Content',
    200,
    array('content-type' => 'application/json; charset=utf-8')
);
$rm = new \Qst\ResourceManager();
$rm->setDriver(new \Qst\Driver\DatabaseDriver());

if ($request->isMethod('post')) {
	$data = $request->get('data');
    $rm->update($data);
    $response->setContent(json_encode(['status'=>'OK']));
} elseif ($request->isMethod('delete')) {
    $data = $request->get('data');
    $rm->delete($data['type'], $data['id']);
} else {
	$response->setContent(json_encode(['status'=>'FAIL', 'message'=>'Method doesn\'t allowed']));
}

$response->send();die();